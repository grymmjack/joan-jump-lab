## Feedback

Yammosk

> Sorry for taking so long to try that out. Life stuff got in the way, but just ran through it now. First of all I have to say that was SOOOOOO good. I loved it. 
> The ASCII effects on the character are gorgeous. I love the trail after the character to signify speed and the death animation is just perfect. The faces on level load spooked me a bit lol, not sure if there is a story element to that, but makes me curious. 
> The level design seems pretty perfect. that last level was really tripping me up, but I think the gradual ramp up was really nice. By the time I got to the last level I really wanted to finish it despite it being hard for me. 
> The movement is really interesting, the fact that is rounded to character positions and with the tight levels felt strange at first. By the end I got the hang of it, and it felt super precise because of that choice. 
> I felt less so about the camera movement. It seems like it snaps to character positions and also has a trailing motion. The combination was was a little uncomfortable for me. I'm curious if you kept one or the other whether that would feel better: ie, camera is locked with the player but still snaps or the camera is smooth, but trails the character. I'm not sure if either of those are stylistic choices, but it also seems with the levels you have so far you could just keep the camera in one place too as another option.
> Overall I think that is just a superb demo of a concept. It's very mechanics heavy and I like it. I would love to see how you built it out.

## If I propose a game and say that you will play a hahstag symbol pushing percentage symbols, will you still play it?

- I wouldn't. The only exception I could think of is if there was some sort of fourth wall breaking element to it; a reason for that choice of art style. I think if you were to stick with such a style you would need some exceptional gameplay or story to draw people in.
- I don't mind the jerky effect, but are you going to put backdrop/decorations below the floor? I would clamp it to the screen edges (plus a margin), at least on the y axis, so the scene is always in frame. Unless the game is built on vertical, I feel like most platformers move the camera more on the primary axis and clamp on the secondary

## Mechanics / levels

moving
simple jump
running
double jump
collectibles

### Levels standard

#### Layers 

Each layer is separeted by a `!` symbol on the file, if a hex value accompany the layer attribution separator, it will be a automatic color change for the layer output `!F00` each layer can contain named sequences `@name of the sequence` and each sequence can contain multiple frames `&` if a number is specified with the frame separator the animation will be automatic `&1000`.

- A or first '!' found: decorative layer, only for visuals
- B or second '!' found: collision layer, only for movement limitation
- C or third '!' found: fatal layer, only for deadly objects
- D (not specified in file): interactive layer, only for event triggering, collectibles, playable characters, events etc.
