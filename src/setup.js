//@COMMON

function isInt(n) {
   return n % 1 === 0;
}

const frame_length = 20, cam = [0, 0, 60, 0], unit = 16, gravity = [0, unit/2], base_font_size = 10;

//@GRAPHICS

const graphics = {};
const assets = {};

function setup_assets(files, i) { 
  if (!i) i = 0;
  if (i == files.length) return "Setup assets done";
  console.log("Setup asset from",files[i]);
  return fetch('./assets/' + files[i]).then(response => {
    return response.text();
  }).then(asset => {
    let asset_lines = asset.split('\n');
    let name = asset_lines[0];
    assets[name] = asset;
    return setup_assets(files, i + 1);
  });
}

Graphic = function(txt) {
  let layers = txt.split('LAYER ');
  
  this.name = layers.shift().replace('\n','');
  this.sequence = "base";
  this.played = 0;
  this.layers = {};

  for (var l = 0; l < layers.length; l++){
    let sequences = layers[l].split('SEQUENCE ');
    let layer_name = sequences.shift().replace('\n','');
    this.layers[layer_name] = {};
    for (var s = 0; s < sequences.length; s++){
      let frames = sequences[s].split('FRAME ')
      let sequence_header = frames.shift().split('\n');
      let sequence_name = sequence_header[0];
      let sequence_mode = sequence_header[1];
      this.layers[layer_name][sequence_name] = {'frame': 0, 'frames': [], 'mode': sequence_mode, "dir": 0};
      for (var f = 0; f < frames.length; f++){
        let frame = frames[f].split('\n');
        let frame_duration = frame.shift().replace('\n',''); 
        this.layers[layer_name][sequence_name]["frames"].push([parseInt(frame_duration), frame, 0]);
      }
    }
  }

  graphics[this.name] = this;
}

// @ELEMENTS

const elements = [];
const elements_to_remove = [];

Element = function(options) {
  if (!options) options = {}

  // MOVEMENT
  this.pos = options.pos? [options.pos[0], options.pos[1]]: [0,0];
  this.velocity = options.velocity? options.velocity: [0,0];
  this.min_speed = options.min_speed? options.min_speed: unit * 0.06;
  this.speed = options.speed? options.speed: unit * 0.06;
  this.max_speed = options.max_speed? options.max_speed: unit * 0.12;
  this.speed_acceleration = options.speed_acceleration? options.speed_acceleration: 1.5;
  this.jumping = true;
  this.jump_speed = 0;
  this.max_jump_speed = options.max_jump_speed? options.max_jump_speed: unit * 0.5;
  this.max_jumps = options.max_jumps? options.max_jumps: 2;
  this.jumps_left = options.max_jumps? options.max_jumps: 0;
  this.jump_speed_decceleration = options.jump_speed_decceleration? options.jump_speed_decceleration: 1.2;
  this.ground_rub = options.ground_rub? options.ground_rub: 1.2;
  this.air_rub = options.air_rub? options.air_rub: 1.2;
  
  // DISPLAY
  this.graphic = new Graphic(options.asset? assets[options.asset]: assets['default']);

  // BEHAVIOUR
  this.behaviour = options.behaviour? options.behaviour: false;
  this.interaction = options.interaction? options.interaction: false;
  this.commands = options.commands? options.commands: false;
 
  elements.push(this);
}

//@BEHAVIOURS

function collide(element, pos){ //@HERE
  let element_layers = element.graphic.layers;
  for (var l in element_layers){
    let sequence = element_layers[l][element.graphic.sequence];
    let frame = sequence['frames'][sequence['frame']]
    let lines = frame[1];
    for (y = 0; y < lines.length; y++){
      for (x = 0; x < lines[y].length; x++){
        let glyph = lines[y][x] 
        if (glyph == ' ') continue;
        let x1 = Math.round(pos[0]/unit) + x;
        let y1 = Math.round(pos[1]/unit) + y;
        let index = String(x1)+','+String(y1);
        if (layers['collision'][index] && layers['collision'][index][1] != element){
          return 'collision';
        }
        if (layers['fatal'][index] && layers['fatal'][index][1] != element){
          return 'fatal';
        }
        if (layers['interactive'][index] && layers['interactive'][index][1] != element){
          return layers['interactive'][index][1];
        }
      }
    }
  }
  return false;
}

function move(element, dir, obstructed) {
  let pos = [element.pos[0] + dir[0], element.pos[1] + dir[1]];
  let collision_x = collide(element,[pos[0],element.pos[1]]);
  let collision_y = collide(element,[element.pos[0],pos[1]]);
  var events = [];
  
  if (collision_x && collision_y) {
    if (obstructed) {
      events.push("fatal_x");
      events.push("fatal_y");
      console.log("COUCOU");
      return events;
    }
    element.pos[0] -= dir[0];
    element.pos[1] -= dir[1];
    collision = collide(element,[element.pos[0],element.pos[1]]);
    if (collision == "collision") move(element, [dir[0]*-1, dir[1]*-1], true);
  }
 
  if (collision_x) {
    if (collision_x == "collision" && !element.jumping && element.jumps_left != element.max_jumps) {
      events.push("collision_x_after_jump");
    } else if (collision_x == "fatal") {
      events.push("fatal_x");
    } else if (collision_x.interaction){
      collision_x.interaction(element,collision_x);
      events.push("interaction_x");
      element.pos[0] += dir[0];
    }
    events.push("collision_x");
  } else {
    element.pos[0] += dir[0];
  }
  if (collision_y) {
    if (collision_y == "collision" && !element.jumping && element.jumps_left != element.max_jumps) {
      events.push("collision_y_after_jump");
    } else if (collision_y == "fatal") {
      events.push("fatal_y");
    } else if (collision_y.interaction){
      collision_y.interaction(element,collision_y);
      events.push("interaction_y");
      element.pos[1] += dir[1];
    }
    events.push("collision_y");
  } else {
    element.pos[1] += dir[1];
  }

  return events;
}

//@JUMP
function jump(element) {
  if (Math.round(element.jump_speed) > 0) {
    element.jump_speed /= element.jump_speed_decceleration;
  } else {
    element.jump_speed = element.max_jump_speed;
    element.jumping = false;
    return false;
  }
  if (gravity[1] > 0) {
    element.velocity[1] -= element.jump_speed;
  } else {
    element.velocity[1] += element.jump_speed;
  }
}

function control_camera(element,commands){
  if (isInt(frame/2)) {
    if (commands['camLeft']) cam[0]--;
    if (commands['camRight']) cam[0]++;
    if (commands['camUp']) cam[1]--;
    if (commands['camDown']) cam[1]++;
  }
  center_cam_on(element);
}

function control(element,commands){
  let events = [];
  if (commands.jump && !element.jumping && element.jumps_left){
    element.jumping = true;
    element.jumps_left--;
    new Element({
      'pos': [element.pos[0]-unit*2,element.pos[1]-unit*2],
      'asset': 'shock',
      'behaviour': function(element) {
        if (element.graphic.played) elements_to_remove.push(this);
      }
    });
    events.push("jump");
  }
  if (element.jumping) { 
    jump(element) 
  } else {
    if (commands.run){
      events.push("run");
    } else {
      events.push("walk");
    }
  }
  if (commands.run && element.speed < element.max_speed) {
    element.speed *= element.speed_acceleration;
  }
  if (commands.right) element.velocity[0] += element.speed;
  if (commands.left) element.velocity[0] -= element.speed;
  let move_events = move(element, element.velocity);
  for (var i = 0; i < move_events.length; i++) {
    events.push(move_events[i]);
  }
  return events;
}

function apply_gravity(element){
  element.velocity[0] /= element.ground_rub;
  element.velocity[1] /= element.air_rub;
  if (element.speed > element.min_speed) element.speed /= element.ground_rub;
  return move(element, gravity);
}

//@CANVAS

const layers = {
  'decorative': {}, 
  'collision': {}, 
  'fatal': {},
  'interactive': {},
}

const outputs = {
  'decorative': document.getElementById('decorative'), 
  'collision': document.getElementById('collision'), 
  'fatal': document.getElementById('fatal'),
  'interactive': document.getElementById('interactive')
}

function update_canvas() {
  // setup layers
  layers['decorative'] = {}
  layers['collision'] = {}
  layers['fatal'] = {}
  layers['interactive'] = {}
  // clean elements
  for (var i = 0; i < elements.length; i++){
    if (elements[i].remove) {
      elements.splice(i,1);
      i-=1;
    }
  }
  // print
  elements.forEach( element => {
    let element_layers = element.graphic.layers;
    for (var l in element_layers){
      let sequence = element_layers[l][element.graphic.sequence];
      let frame = sequence['frames'][sequence['frame']]
      let lines = frame[1];
      
      //START ANIMATION
      var mode = sequence['mode'];
      let dir = sequence['dir'];
      if (frame[0]){
        if (mode == 'ALTERNATE') {
          if (!dir) {
            mode = "FORWARD";
          } else {
            mode = "BACKWARD";
          }
        }
        if (mode == 'FORWARD') {
          frame[2]++;
          if (frame[2] >= frame[0]) {
            sequence.frame++;
            frame[2] = 0;
          }
          if (sequence.frame >= sequence.frames.length) {
            sequence.frame = 0;
            element.graphic.played ++;
            if (sequence['mode'] == 'ALTERNATE'){
              sequence['dir'] = 1;
              sequence.frame = sequence.frames.length-2;
            }
          }
        } else if (mode == 'BACKWARD') {
          frame[2]++;
          if (frame[2] >= frame[0]) {
            sequence.frame--;
            frame[2] = 0;
          }
          if (sequence.frame < 0) {
            sequence.frame = sequence.frames.length-1;
            element.graphic.played ++;
            if (sequence['mode'] == 'ALTERNATE'){
              sequence['dir'] = 0;
              sequence.frame = 1;
            }
          }
        }
      }
      //END ANIMATION

      for (y = 0; y < lines.length; y++){
        for (x = 0; x < lines[y].length; x++){
          let glyph = lines[y][x] 
          if (glyph == ' ') continue;
          let x1 = Math.round(element.pos[0]/unit) + x;
          let y1 = Math.round(element.pos[1]/unit) + y;
          layers[l][x1+','+y1] = [glyph,element];
        }
      }
    }
  });
}

function print_canvas(){
  for (var l in layers) {
    let text_content = '';
    for (var y = cam[1]; y < cam[3]+cam[1]; y++) {
      for (var x = cam[0]; x < cam[2]+cam[0]; x++) {
        let masks = false;
        if (l == 'decorative') masks = ['collision','fatal','interactive'];
        if (l == 'fatal') masks = ['collision'];
        if (masks){
          for (var i = 0; i <  masks.length; i++) {
            if (layers[masks[i]][x+','+y]) layers[l][x+','+y] = false; 
          }
        }
        if (layers[l][x+','+y]) {
          text_content += layers[l][x+','+y][0]; 
        } else {
          text_content += ' '; 
        }
      }
      text_content += '\n';
    }
    if (outputs[l].textContent != text_content) {
      outputs[l].textContent = text_content;
    }
  }
}

//@DOM

function setup_dom(){
  let font_size_multiplicator = parseInt(window.innerWidth / (base_font_size/2) / cam[2]);
  cam[2] = parseInt(window.innerWidth / ((base_font_size / 2) * font_size_multiplicator));
  cam[3] = parseInt(window.innerHeight / (base_font_size * font_size_multiplicator));
  for (var i in outputs) {
    outputs[i].style.fontSize = font_size_multiplicator+'rem'; 
    outputs[i].style.lineHeight = font_size_multiplicator+'rem'; 
  }
};

//@CAMERA

function center_cam_on(element){
  if (Math.round(element.pos[0] / unit) - cam[0] > Math.round(cam[2] - cam[2] / 3)) {
    let diff = (Math.round(element.pos[0] / unit) - cam[0]) - Math.round(cam[2] - cam[2] / 3);
    cam[0] += Math.round(diff);
  } else if (Math.round(element.pos[0] / unit) - cam[0] < Math.round(cam[2] / 3)) {
    let diff = (Math.round(element.pos[0] / unit) - cam[0]) - Math.round(cam[2] / 3);
    cam[0] += Math.round(diff);
  }
  if (Math.round(element.pos[1] / unit) - cam[1] > Math.round(cam[3] - cam[3] / 3)) {
    let diff = (Math.round(element.pos[1] / unit) - cam[1]) - Math.round(cam[3] - cam[3] / 3);
    cam[1] += Math.round(diff);
  } else if (Math.round(element.pos[1] / unit) - cam[1] < Math.round(cam[3] / 3)) {
    let diff = (Math.round(element.pos[1] / unit) - cam[1]) - Math.round(cam[3] / 3);
    cam[1] += Math.round(diff);
  }
}

//@TIME

var frame = 0;

function time(game_speed){
  frame++;
  // behaviour
  elements.forEach( element => {
    if (element.behaviour) element.behaviour(element);
  });
  // remove dead elements
  while (elements_to_remove.length) {
    elements.splice(elements.indexOf(elements_to_remove[0]),1);
    elements_to_remove.shift();
  }
  update_canvas();
  print_canvas();
  setTimeout(function(){
    time(game_speed);
  }, game_speed);
}

// @Inputs


//@COMMANDS

function new_commands(){
  return {
    'camLeft': false,
    'camRight': false,
    'camUp': false,
    'camDown': false,
    'left': false,
    'right': false,
    'run': false,
    'jump': false
  }
}

const commands = [
  new_commands(),
  new_commands(),
]

const key_map = {
  'j': [0,'camLeft'],
  'l': [0,'camRight'],
  'i': [0,'camUp'],
  'k': [0,'camDown'],
  'control': [0,'run'],
  'arrowleft': [0,'left'],
  'arrowright': [0,'right'],
  'arrowup': [0,'jump'],
  'control': [0,'run'],
  'q': [1,'left'],
  'd': [1,'right'],
  'z': [1,'jump'],
  'shift': [1,'run']
}

document.addEventListener('keydown', function(e){
  let key = e.key.toLowerCase();
  if (key_map[key]) commands[key_map[key][0]][key_map[key][1]] = true;
});

document.addEventListener('keyup', function(e){
  let key = e.key.toLowerCase();
  if (key_map[key]) commands[key_map[key][0]][key_map[key][1]] = false;
});


//@SETUP

var level, cam_focus, player1;

setup_dom();
fetch('./assets/index.txt').then(response => {
  return response.text();
}).then(txt => {
  let files = txt.split('\n');
  files.pop();
  files.splice(files.indexOf('index.txt'), 1);
  return files;
}).then(files => {
  setup_assets(files).then(msg => {
    console.log(msg);
    // LEVEL
    level = new Element({'asset': 'acid_2'});
    // KEY
    // new Element({
    //   'pos': [47*unit,3*unit],
    //   'asset': 'key',
    //   'interaction': function(from, to) {
    //     elements_to_remove.push(to);
    //     level.graphic.sequence = 'end';
    //     new Element({
    //       'pos': [to.pos[0],to.pos[1]-unit*3],
    //       'asset': 'explosion',
    //       'behaviour': function(element) {
    //         if (element.graphic.played) elements_to_remove.push(element);
    //       }
    //     });
    //   }
    // });
    // PLAYER1
    player1 = new Element({
      'pos': [1*unit,12*unit],
      'asset': 'player',
      'behaviour': function(element) {
        let events = [];
        let gravity_events = apply_gravity(element);
        let control_events = control(element,commands[0]);
        for (var i = 0; i < gravity_events.length; i++){
          events.push(gravity_events[i]);
        }
        for (var i = 0; i < control_events.length; i++){
          events.push(control_events[i]);
        }
        if (events.indexOf("walk") > -1) {
          new Element({
            'pos': [element.pos[0],element.pos[1]],
            'asset': 'small_trail',
            'behaviour': function(element) {
              if (element.graphic.played) elements_to_remove.push(this);
            }
          });
        }
        if (events.indexOf("run") > -1) {
          new Element({
            'pos': [element.pos[0],element.pos[1]],
            'asset': 'trail',
            'behaviour': function(element) {
              if (element.graphic.played) elements_to_remove.push(this);
            }
          });
        }
        if (events.indexOf("fatal_y") > -1   
         || events.indexOf("fatal_x") > -1) {
          console.log("COUCOU_fatal");
          elements_to_remove.push(element);
          new Element({
            'pos': [element.pos[0]-unit*3,element.pos[1]-unit*3],
            'asset': 'explosion',
            'behaviour': function(element) {
              if (element.graphic.played) elements_to_remove.push(element);
            }
          });
        }
        if (events.indexOf("collision_x_after_jump") > -1
         || events.indexOf("collision_y_after_jump") > -1) {
          element.jumps_left = element.max_jumps; 
          new Element({
            'pos': [element.pos[0]-unit,element.pos[1]-unit],
            'asset': 'small_shock',
            'behaviour': function(element) {
              if (element.graphic.played) elements_to_remove.push(this);
            }
          });
        }
        control_camera(element,commands[0]);
      },'interaction': function(from, to) {
        let collision = collide(to, from.pos);
        if (collision == "fatal") {
          elements_to_remove.push(to);
          new Element({
            'pos': [to.pos[0]-unit*3,to.pos[1]-unit*3],
            'asset': 'explosion',
            'behaviour': function(element) {
              if (element.graphic.played) elements_to_remove.push(element);
            }
          });
        }
      }
    });
    cam_focus = player1;
    // ENEMY
    var enemy = new Element({
      'pos': [10*unit,5*unit],
      'asset': 'robot',
      'min_speed': unit * 0.015,
      'speed': unit * 0.015,
      'max_speed': unit * 0.3,
      'commands': new_commands(),
      'behaviour': function(element) {
        if (element.graphic.sequence == "end") return;
        apply_gravity(element);
        control(element,element.commands);
        if (element.commands.left) {
          let collision = collide(element, [element.pos[0]-unit*2, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = false;
            element.commands['right'] = true;
          }
        } else if (element.commands.right){
          let collision = collide(element, [element.pos[0]+unit*2, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = true;
            element.commands['right'] = false;
          }
        }
      },
      'interaction': function(from, to) {
        // if (from.jumps_left == from.max_jumps) return;
        to.graphic.sequence = "end";
        new Element({
          'pos': [to.pos[0]+unit*4,to.pos[1]-unit*3],
          'asset': 'explosion',
          'behaviour': function(element) {
            if (element.graphic.played) elements_to_remove.push(element);
          }
        });
      }
    });
    enemy.commands['left'] = true;
    // ENEMY
    var enemy = new Element({
      'pos': [50*unit,5*unit],
      'asset': 'robot',
      'min_speed': unit * 0.015,
      'speed': unit * 0.015,
      'max_speed': unit * 0.3,
      'commands': new_commands(),
      'behaviour': function(element) {
        if (element.graphic.sequence == "end") return;
        apply_gravity(element);
        control(element,element.commands);
        if (element.commands.left) {
          let collision = collide(element, [element.pos[0]-unit*2, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = false;
            element.commands['right'] = true;
          }
        } else if (element.commands.right){
          let collision = collide(element, [element.pos[0]+unit*2, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = true;
            element.commands['right'] = false;
          }
        }
      },
      'interaction': function(from, to) {
        // if (from.jumps_left == from.max_jumps) return;
        to.graphic.sequence = "end";
        new Element({
          'pos': [to.pos[0]+unit*4,to.pos[1]-unit*3],
          'asset': 'explosion',
          'behaviour': function(element) {
            if (element.graphic.played) elements_to_remove.push(element);
          }
        });
      }
    });
    enemy.commands['left'] = true;
    // ENEMY
    var enemy = new Element({
      'pos': [75*unit,5*unit],
      'asset': 'robot',
      'min_speed': unit * 0.015,
      'speed': unit * 0.015,
      'max_speed': unit * 0.3,
      'commands': new_commands(),
      'behaviour': function(element) {
        if (element.graphic.sequence == "end") return;
        apply_gravity(element);
        control(element,element.commands);
        if (element.commands.left) {
          let collision = collide(element, [element.pos[0]-unit*2, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = false;
            element.commands['right'] = true;
          }
        } else if (element.commands.right){
          let collision = collide(element, [element.pos[0]+unit*2, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = true;
            element.commands['right'] = false;
          }
        }
      },
      'interaction': function(from, to) {
        // if (from.jumps_left == from.max_jumps) return;
        to.graphic.sequence = "end";
        new Element({
          'pos': [to.pos[0]+unit*4,to.pos[1]-unit*3],
          'asset': 'explosion',
          'behaviour': function(element) {
            if (element.graphic.played) elements_to_remove.push(element);
          }
        });
      }
    });
    enemy.commands['left'] = true;
    // ENEMY
    var enemy = new Element({
      'pos': [74*unit,13*unit],
      'asset': 'enemy',
      'min_speed': unit * 0.03,
      'speed': unit * 0.03,
      'max_speed': unit * 0.6,
      'commands': new_commands(),
      'behaviour': function(element) {
        apply_gravity(element);
        control(element,element.commands);
        if (element.commands.left) {
          let collision = collide(element, [element.pos[0]-unit, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = false;
            element.commands['right'] = true;
          }
        } else if (element.commands.right){
          let collision = collide(element, [element.pos[0]+unit, element.pos[1]]);
          if (collision == "collision" || collision == "fatal") {
            element.commands['left'] = true;
            element.commands['right'] = false;
          }
        }
      },
      'interaction': function(from, to) {
        // if (from.jumps_left == from.max_jumps) return;
        elements_to_remove.push(to);
        new Element({
          'pos': [to.pos[0]-unit*3,to.pos[1]-unit*3],
          'asset': 'explosion',
          'behaviour': function(element) {
            if (element.graphic.played) elements_to_remove.push(element);
          }
        });
      }
    });
    enemy.commands['left'] = true;
    time(17);
  });
});
